source "$(dirname $0)/settings.sh"


for project in $(find $WORKSPACE -type d -name ".git")
do
    cd $(dirname $project)
    git add -A
    git commit -m "auto commit from @gwikgwik"
    git push
done
